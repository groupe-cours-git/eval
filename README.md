Évaluation Git 2024
===================

*Durée prévue: 1h.*

*Tiers-temps: 6 questions théoriques (au choix) seront évaluées. La
partie pratique reste identique.*

L'évaluation porte sur une partie pratique et une partie théorique. Elle
est individuelle (pas de communication entre vous), et vous avez le
droit aux cours et documentations.

Préparation
-----------

1.  Faire un *fork* de ce dépôt dans votre compte Gitlab, en gardant un
    **niveau de visibilité privé**.
2.  Depuis Gitlab, sur la page de votre dépôt, dans la barre de gauche,
    aller sur « **Project information** », cliquer sur « **Members** »,
    puis sur la page qui s'affiche, « **Invite members** ». Dans la
    fenêtre qui s'ouvre, taper `jeanjil`, cliquer sur mon compte,
    sélectionner le rôle de « **Developer** », et valider en cliquant
    sur « **Invite** ».

*Si cette partie pose problème, lever la main et attendre que j'arrive.*

3.  Cloner ce dépôt sur votre ordinateur.

Théorie
-------

Sur votre ordinateur, éditez ce fichier pour répondre (en sautant des
lignes entre les questions) aux questions suivantes :

4.  Quel est l'intérêt du fichier `README.md` dans un dépôt Git ?


5.  Je suis dans le dossier `~/projets/circuit/` et j'entre la commande
    `git init ../mon_depot`. Que se passe-t-il ?


6.  Quand on clone un projet, quel est le nom du remote qui est créé
    automatiquement ?


7.  Quelle commande vous permet de *modifier* le dernier commit ?


8.  À quoi sert un tag (donner un exemple d'utilisation) ?


9.  Si l'on crée un dépôt en local *sans le cloner depuis GitLab*,
    peut-on « envoyer » ce dépôt sur GitLab ? Si oui, détailler comment
    faire. Si non, donner la raison.


10. Comment utiliser Git pour collaborer avec un collègue sur un projet ?
    Détailler les étapes à mettre en place puis les étapes normales
    d'un travail en collaboration avec Git (illustrer avec les commandes
    nécessaires).


11. J'utilise `git push` et le résultat est
    ```
    To git@gitlab.com:mylogin/myproject.git
     ! [rejected]       main -> main (fetch first)
    error: failed to push some refs to 'git@gitlab.com:mylogin/myproject'
    ```
    Que dois-je faire avant de retenter `git push`?


12. Quelle commande est utilisée pour « indexer » des fichiers (les
    mettre dans la zone d'indexation) ? Dans quelle situation
    pourrait-il être utile de ne pas mettre tous les fichiers modifiés
    dans cette zone ?


13. La commande `git status` me donne le résultat suivant :
    ```
    Changes to be committed:
      (use "git restore --staged <file>..." to unstage)
          new file:   Cours_Stats.tex

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git restore <file>..." to discard changes in working directory)
          modified:   Cours_Proba.tex
    ```
    Quel sera le résultat de la commande `git commit -m "Un message"` ?

Pratique
--------

14. Valider les modifications de ce fichier en effectuant un commit.
15. Pousser votre (ou vos) commit(s) sur votre dépôt Gitlab, vérifier
    que vos réponses apparaissent convenablement.
16. Avec les commandes Git vues en cours (et / ou en cherchant dans la
    doc si besoin), manipuler votre dépôt pour que le résultat de la
    commande `git log --oneline --graph --all` ressemble à ça :
    ```
    * eacd503 (dev1) Commit 5
    | * cb0b397 (HEAD -> main, tag: v1) Commit 4
    | | * 5c86e1d (dev2) Commit 3
    | |/
    |/|
    * | e2b5eed Commit 2
    |/
    * 10421d3 (tag: v0) Commit 1
    ```
    Seuls les 5 derniers commits seront pris en compte. L'ordre des
    commits, les branches, tags, et la forme du graphe ont de
    l'importance, mais pas les SHA-1 qui apparaissent.
17. Pousser toutes les branches et tags sur votre dépôt distant en
    tapant : `git push --all && git push --tags`.

Avant de partir, vérifier qu'il n'y a pas d'erreur et que la liste de
tous vos commits apparaît sur votre dépôt Gitlab (Voir dans « Repository
→ Commits » ou mieux, « Repository → Graph »). **Attention** : ce qui
n'est pas dans votre dépôt distant ne sera pas pris en compte (parce que
je ne le verrai pas).
